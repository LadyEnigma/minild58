﻿using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour
{
    public Rigidbody thisBody;             //the attached rigidbody component
    private AudioSource sound;               //the attached audio source component

    public GameManager gameManager;         //gameManager script
    public GameObject thisTwister = null;   //the current twister the ball is caught in
    public bool inTwister = false;          //is the ball caught by the twister?

    private Vector3 lastTwisterPosition = Vector3.zero;     //the last position of this twister
    private Vector3 currentTwisterDelta = Vector3.zero;     //the change over time of this twisters movement
    
    private float directionX;               //direction change in X
    private float directionZ;               //direction change in Z
    
    public void Awake()
    {
        //get the attached rigidbody component
        thisBody = GetComponent<Rigidbody> ();
        //get the attached audio source compoenent
        sound = GetComponent<AudioSource> ();
        //find the game manager
        gameManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager> ();

        //set direction in X and Y randomly
        directionX = Random.Range (0, 2) == 0 ? -1 : 1;
        directionZ = Random.Range (0, 2) == 0 ? -1 : 1;
        
        //start movement
        thisBody.velocity = new Vector3 (Random.Range (5, 20) * directionX, 0, Random.Range (5, 20) * directionZ);
    }

    void FixedUpdate()
    {
        //set random direction change for next collision
        directionX = Random.Range (0, 2) == 0 ? -1 : 1;
        directionZ = Random.Range (0, 2) == 0 ? -1 : 1;

        //reset ball when out of game bounds
        if (transform.position.z > 25.1 || transform.position.z < -25.1)
        {
            gameManager.NewBall ();
        }

        if (inTwister)
        {
            //stop ball velocity
            thisBody.velocity = Vector3.zero;
            //reset delta
            currentTwisterDelta = Vector3.zero;
            //set the last position of the twister to its current position
            lastTwisterPosition = thisTwister.transform.position;
            Invoke ("TwisterDrop", 5);
        }
    }

    void TwisterDrop()
    {
        Debug.Log ("Called Drop");
        //the ball is no longer caught by the twister
        inTwister = false;
        //reset this twister
        thisTwister = null;
        //restart the balls velocity
        thisBody.velocity = thisBody.velocity = new Vector3 (Random.Range (5, 20) * directionX, 0, Random.Range (5, 20) * directionZ);
        
    }

    void LateUpdate()
    {
        if (thisTwister != null)
        {
            //find the amount the twister has moved since the last update
            currentTwisterDelta = thisTwister.transform.position - lastTwisterPosition;
            //set the last position of the twister to its current position
            lastTwisterPosition = thisTwister.transform.position;
            //move the ball with the twister
            transform.position += currentTwisterDelta;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //change direction and speed randomly on collision
        thisBody.velocity = new Vector3 (Random.Range (5, 30) * directionX, 0, Random.Range (5, 30) * directionZ);
        if (other.gameObject.name != "Floor")
        {
            sound.Play ();
        }
    }

    void OnTriggerEnter(Collider scoreTo)
    {
        if (scoreTo.name == "PlayerScoreBox")
        {
            if (gameManager.scored == false)
            {
                //score a point to the enemy
                Debug.Log ("Enemy scored a point");
                gameManager.UpdateEnemyScore (); 
            }
        }
        if (scoreTo.name == "EnemyScoreBox")
        {
            if (gameManager.scored == false)
            {
                //score a point to the player
                Debug.Log ("Player scored a point");
                gameManager.UpdatePlayerScore (); 
            }
        }
        if (scoreTo.name == "Twister")
        {
            Debug.Log ("Twister got the ball");
            inTwister = true;
            //set this twister to the one we are caught in
            thisTwister = scoreTo.gameObject;
        }
    }
}
