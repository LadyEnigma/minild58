﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour
{
    public GameObject target;               //Target object to follow
    public float speed = 5.0f;              //speed to move


    void Start()
    {
        //find the ball target
        target = GameObject.FindGameObjectWithTag ("Ball");
    }

    void Update()
    {
        //get the enemy current position
        Vector3 newPosition = transform.position;
        //set the target position to the position of the ball
        Vector3 targetPosition = target.transform.position;
        //move toward the target
        newPosition.x += targetPosition.x * 1.5f; // *speed * Time.deltaTime;
        //keep the enemy within the game bounds
        if (newPosition.x < -17) newPosition.x = -17;
        if (newPosition.x > 17) newPosition.x = 17;
        //apply the movement
        transform.position = Vector3.Lerp (transform.position, newPosition, speed * Time.deltaTime);
    }
}
