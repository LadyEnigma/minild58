﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public BallControl theBall;               //the ball control script
    public EnemyControl enemyControl;        //Enemy Control script
    public Text yourScore;                  //player score text
    public Text foeScore;                   //enemy score text
    public Text plusOne;                    //text that shows you got a point
    public Text missed;                     //text that shows you let the enemy score
    public GameObject enemyScoreBox;        //enemy goal scoring box
    public GameObject playerScoreBox;       //player goal scoring box
    public AudioClip scoreSound;            //sound to play when player scores
    public AudioClip loseSound;             //sound to play when enemy scores
    private MeshRenderer enemyBox;          //enemy score box renderer
    private MeshRenderer playerBox;         //player score box renderer
    private int playerScore = 0;             //players score
    private int enemyScore = 0;              //enemys score
    private AudioSource sound;              //the attached audio source component

    public bool scored = false;            //did we just score?

    void Start ()
    {
        plusOne.enabled = false;
        missed.enabled = false;
        enemyBox = enemyScoreBox.GetComponent<MeshRenderer> ();
        playerBox = playerScoreBox.GetComponent<MeshRenderer> ();
        sound = GetComponent<AudioSource> ();
        enemyBox.enabled = true;
        playerBox.enabled = true;
    }

    public void UpdatePlayerScore ()
    {
        //increment the player score
        enemyBox.enabled = false;
        sound.clip = scoreSound;
        sound.volume = 0.25f;
        sound.Play ();
        playerScore++;
        yourScore.text = "You: " + playerScore;
        plusOne.enabled = true;
        scored = true;
    }

    public void UpdateEnemyScore ()
    {
        //increment the enemy score
        playerBox.enabled = false;
        sound.clip = loseSound;
        sound.volume = 0.5f;
        sound.Play ();
        enemyScore++;
        foeScore.text = "Foe: " + enemyScore;
        missed.enabled = true;
        scored = true;
    }

    public void NewBall ()
    {
        Debug.Log ("Waiting..");
        //pause before getting a new ball
        Invoke ("ResetBall", 1);
    }

    void ResetBall ()
    {
        Debug.Log ("Reset the ball");
        //acting like we spawned a new ball
        theBall.Awake ();
        //reset the ball position
        theBall.transform.position = Vector3.zero;
        plusOne.enabled = false;
        missed.enabled = false;
        enemyBox.enabled = true;
        playerBox.enabled = true;
        scored = false;
    }

}
