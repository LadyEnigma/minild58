﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour 
{
    public float speed = 5.0f;                  //movement speed

    void Update()
    {
        //get current position
        Vector3 newPosition = transform.position;
        //move based on user input
        newPosition.x += Input.GetAxis ("Horizontal");
        
        //keep the player within the game bounds
        if (newPosition.x < -19) newPosition.x = -19;
        if (newPosition.x > 19) newPosition.x = 19;

        //apply the movement
        transform.position = newPosition;
    }
}
