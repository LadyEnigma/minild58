﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour
{
    public void PushStart()
    {
        Application.LoadLevel ("TestLevel");
    }

    public void ShowCredits()
    {
        Application.LoadLevel ("Credits");
    }

    public void ShowMenu()
    {
        Application.LoadLevel ("MenuStart");
    }
}
